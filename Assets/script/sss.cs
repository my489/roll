using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeCut : MonoBehaviour
{
    public static bool Cut(Transform victim, Vector3 _pos)
    {
        Vector3 pos = new Vector3(victim.position.x, _pos.y, victim.position.z);
        Vector3 victimScale = victim.localScale;
        float distance = Vector3.Distance(victim.position, pos);
        if (distance >= victimScale.y / 2) return false;

        Vector3 leftPoint = victim.position - Vector3.up * victimScale.y / 2;
        Vector3 rightPoint = victim.position + Vector3.up * victimScale.y / 2;
        Material mat = victim.GetComponent<MeshRenderer>().material;
        Destroy(victim.gameObject);

        GameObject rightSideObj = GameObject.CreatePrimitive(PrimitiveType.Cube);
        rightSideObj.transform.position = (pos + victim.position + new Vector3(0, 1, Random.Range(3, -3)) * victimScale.y / 2) / 2;
        //rightSideObj.transform.position = (pos + victim.position + new Vector3(0, 1, -1) * victimScale.y / 2) / 2;
        float rightWidth = Vector3.Distance(pos, rightPoint);
        rightSideObj.transform.localScale = new Vector3(victimScale.x, rightWidth, victimScale.z);
        rightSideObj.AddComponent<Rigidbody>().mass = 0.1f;
        rightSideObj.GetComponent<MeshRenderer>().material = mat;

        GameObject leftSideObj = GameObject.CreatePrimitive(PrimitiveType.Cube);
        leftSideObj.transform.position = (leftPoint + pos) / 2;
        float leftWidth = Vector3.Distance(pos, leftPoint);
        leftSideObj.transform.localScale = new Vector3(victimScale.x, leftWidth, victimScale.z);
        //leftSideObj.AddComponent<Rigidbody>().mass = 100f;
        leftSideObj.GetComponent<MeshRenderer>().material = mat;

        return true;
    }
}